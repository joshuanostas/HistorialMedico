import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { MedicalInfo } from "../interfaces/medical-info.interface";
import { Http, Headers } from "@angular/http";
import { AngularFireDatabaseModule, AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase';
import { Observable } from "rxjs/Observable";
import "rxjs/Rx";

@Injectable()
export class MedicalInfoService {

  public itemRef: AngularFireList<any>;
  public itemRef2: AngularFireList<any>;
  public userId : any;
  public urlMedicalInfos = "https://historialmedico-74f5f.firebaseio.com/medicalInfos";

  constructor(private db: AngularFireDatabase,
              private router: Router,
              public afAuth : AngularFireAuth,
              private http:Http){
                this.afAuth.authState.subscribe(user =>{
                  this.userId = user.uid;
                  this.getMedicalInfos(this.userId);
                  this.itemRef = this.db.list("medicalInfos/"+this.userId);
              });
              }

  getMedicalInfos(id): Observable<any[]>{
    this.itemRef = this.db.list('medicalInfos/'+id);
   return this.itemRef.snapshotChanges().map(value =>{
     return value.map(val => ({key: val.payload.key, ... val.payload.val()})
  );});
  }

  getMedicalInfos2(id,profileId): Observable<any[]>{
    this.itemRef = this.db.list('medicalInfos/'+id+'/'+profileId);
    return this.itemRef.snapshotChanges().map(value =>{
      return value.map(val => ({key: val.payload.key, ... val.payload.val()})
    );
    });
  }
  
  updateMedicalInfo(profileId:string, key: string, newMedicalInfo: MedicalInfo) {
    this.itemRef = this.db.list('medicalInfos/' + this.userId + '/' + profileId);
    this.itemRef.update(key, newMedicalInfo);
    this.router.navigate(["/profileList"]);
  }

  getMedicalInfo(profileId:string, key:string){
    let url = `${this.urlMedicalInfos}/${this.userId}/${profileId}/${key}.json`;
    return this.http.get( url)
    .map(res => res.json());
  }

  createMedicalInfo(medicalInfo: MedicalInfo, profileId:string){
    this.itemRef2 = this.db.list("medicalInfos/" + this.userId + "/" + profileId);
    this.itemRef2.push(medicalInfo);
    this.router.navigate(['/medicallist/' + profileId]);
  }

  deleteMedicalInfo(profileId, key:string){
    this.itemRef = this.db.list('medicalInfos/' + this.userId + '/' + profileId);
    this.itemRef.remove(key);
  }

}
