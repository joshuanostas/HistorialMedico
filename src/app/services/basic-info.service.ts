import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { Http, Headers } from "@angular/http";
import { BasicInfo } from "../interfaces/basic-info.interface";
import { AngularFireDatabaseModule, AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import "rxjs/Rx";

@Injectable()
export class BasicInfoService {

  public profileId: string;
  public itemRef: AngularFireList<any>;
  public itemRef2: AngularFireList<any>;
  public userId: any = null;
  public urlBasicInfos = "https://historialmedico-74f5f.firebaseio.com/basicInfos";

  constructor(private db: AngularFireDatabase,
              private http: Http,
              public afAuth : AngularFireAuth,
              private router: Router ){
                this.afAuth.authState.subscribe(user =>{
                    this.userId = user.uid;
                    this.getBasicInfos(this.userId);
                    this.itemRef = this.db.list("basicInfos/"+user.uid);
                });
              }  
 
  getBasicInfos(id): Observable<any[]>{
    this.itemRef = this.db.list('basicInfos/'+id);
    return this.itemRef.snapshotChanges().map(value =>{
      return value.map(val => ({key: val.payload.key, ... val.payload.val()})
    );
    });
  }

  getBasicInfos2(id,profileId): Observable<any[]>{
    this.itemRef = this.db.list('basicInfos/'+id+'/'+profileId);
    return this.itemRef.snapshotChanges().map(value =>{
      return value.map(val => ({key: val.payload.key, ... val.payload.val()})
    );
    });
  }

  updateBasicInfo(profileId , key: string, newBasicInfo: BasicInfo) {
    this.itemRef = this.db.list('basicInfos/'+this.userId+'/'+profileId);
    this.itemRef.update(key, newBasicInfo);
    this.router.navigate(["/profileList"]);
  }
  
  createBasicInfo( basicInfo: BasicInfo, profileId: string){
    this.itemRef2 = this.db.list("basicInfos/" + this.userId + "/" + profileId);
    this.itemRef2.push(basicInfo);
    this.router.navigate(['/medicalinfo/' + profileId]);
    this.db.list("basicInfos/" + this.userId + "/" + profileId).snapshotChanges().map(value =>{
      value.map(val => console.log(val.payload.key));
  })
  }

  getBasicInfo(key:string, profileId:string){
    let url = `${this.urlBasicInfos}/${this.userId}/${profileId}/${key}.json`;
    console.log(url);
    return this.http.get(url)
    .map(res => res.json());
  }

  deleteBasicInfo(profileId,key:string){
    this.itemRef = this.db.list("basicInfos/" + this.userId + "/" + profileId);
    this.itemRef.remove(key);
  }

}
