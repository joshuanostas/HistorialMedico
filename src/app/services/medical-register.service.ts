import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { MedicalRegister } from "../interfaces/medical-register.interface";
import { Http, Headers } from "@angular/http";
import { AngularFireDatabaseModule, AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase';
import { Observable } from "rxjs/Observable";
import "rxjs/Rx";

@Injectable()
export class MedicalRegisterService {

  profileId:string;
  itemsRef: AngularFireList<any>;
  userId : any;
  urlMedicalRegisters = "https://historialmedico-74f5f.firebaseio.com/medicalRegisters";

  constructor(
              private db:AngularFireDatabase,
              private router: Router,
              public afAuth : AngularFireAuth,
              private http:Http ) {
                this.afAuth.authState.subscribe(user =>{
                  this.userId = user.uid;
                  //this.getMedicalRegisters(this.userId);
                  //this.itemsRef = this.db.list("medicalRegisters/"+this.userId);
              });

               }

  getMedicalRegister(key:string) {
    let url = `${this.urlMedicalRegisters}/${this.userId}/${this.profileId}/${key}.json`;
      return this.http.get( url)
      .map(res => res.json());
  }

  updateMedicDocument(key: string, newmedicalRegister: MedicalRegister) {
    this.itemsRef.update(key, newmedicalRegister);
    this.router.navigate(["/medicallist/" + this.profileId]);
  }

  getMedicalRegisters(id , profileId ): Observable<any[]>{
    this.profileId = profileId;
    this.itemsRef = this.db.list("medicalRegisters/" + id + "/" + this.profileId );
    return this.itemsRef.snapshotChanges().map(value => { 
      return value.map(val => ({key: val.payload.key, ... val.payload.val()})
      );
  });
  }

  createMedicalRegister(medicalRegister: MedicalRegister){
    this.itemsRef.push(medicalRegister);
    this.router.navigate(['/medicallist/' + this.profileId]);
  }

  deleteMedicalRegister(key:string){
    this.itemsRef.remove(key);
  }

}
