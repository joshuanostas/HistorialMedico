import { Injectable } from '@angular/core';
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";

import "rxjs/add/operator/map";

@Injectable()
export class AuthService {
  constructor(
    public afAuth: AngularFireAuth
  ) { }

  registerUser(email:string, password:string){
    return new Promise((resolve, reject)=>{
      this.afAuth.auth.createUserAndRetrieveDataWithEmailAndPassword(email,password)
      .then(userData =>resolve(userData),
      err => reject(err));
    });
  }

  refresh(): void {
    window.location.reload();
  } 

  loginEmail(email:string, password:string){
    return new Promise((resolve, reject)=>{
      this.afAuth.auth.signInWithEmailAndPassword(email,password)
      .then(userData => resolve(userData),
      err => reject(err));
    });
  }

  resetPassword(email:string){
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
    .then(()=> alert("email enviado"))
    .catch((error) => console.log(error))
  }

  getAuth(){
    return this.afAuth.authState.map( auth=> auth);
  }

  changePassword(email){
    var auth = firebase.auth();
    return auth.currentUser.updatePassword(email)
    .then(()=> alert("Contraseña actualizada"))
    .catch((error) => console.log(error))
  }

  logout(){
    return this.afAuth.auth.signOut();
  }
}
