export interface MedicalRegister{
    key?: string;
    profileId:string;
    photo? :string;
    title: string;
    date: Date;
    doctorName: string;
    hospital: string;
    speciality: string;
    registerType: string;
    description: string;
    analysis: string;
    prescription: string;
    commentary:string;
}