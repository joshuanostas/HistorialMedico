export interface BasicInfo{
    key?: string;
    profileId:string;
    photo?: string;
    firstName: string;
    lastName: string;
    date: Date;
    gender: string;
    direction: string;
    phone: string;
    email: string;
}