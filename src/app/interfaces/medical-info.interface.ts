export interface MedicalInfo{
    key?: string;
    profileId:string;
    height: string;
    weight: string;
    blood: string;
    allergies: string;
    diseases: string;
    treatments: string;
}