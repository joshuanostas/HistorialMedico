import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


//Componentes
import { PrincipalIuComponent } from './components/principal-iu/principal-iu.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { BasicInfoPageComponent } from './components/crud basic-info/basic-info-page/basic-info-page.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { MedicalInfoPageComponent } from './components/crud medical-info/medical-info-page/medical-info-page.component';
import { MedicalInfoViewComponent } from './components/crud medical-info/medical-info-view/medical-info-view.component';
import { MedicalInfoEditComponent } from './components/crud medical-info/medical-info-edit/medical-info-edit.component';
import { BasicInfoViewPageComponent } from './components/crud basic-info/basic-info-view-page/basic-info-view-page.component';
import { BasicInfoEditComponent } from './components/crud basic-info/basic-info-edit/basic-info-edit.component';
import { MedicalRegisterPageComponent } from "./components/crud medical-register/medical-register-page/medical-register-page.component";
import { MedicalRegisterListComponent } from "./components/crud medical-register/medical-register-list/medical-register-list.component";
import { MedicalRegisterViewComponent } from "./components/crud medical-register/medical-register-view/medical-register-view.component";
import { MedicalRegisterEditComponent } from "./components/crud medical-register/medical-register-edit/medical-register-edit.component";
import { ProfileListComponent } from './components/profile-list/profile-list.component';
import { HtmlToPdfComponent } from './components/html-to-pdf/html-to-pdf.component';

//Guards
import { AuthGuard } from "./guards/auth.guard";

const routes: Routes = [
  { path: '', component: PrincipalIuComponent},
  { path: 'login', component: LoginPageComponent},
  { path: 'register', component: RegisterPageComponent },
  { path: 'profileList', component: ProfileListComponent, canActivate:[AuthGuard] },
  { path: 'basicinfo', component: BasicInfoPageComponent, canActivate:[AuthGuard] },
  { path: 'viewBasicInfo/:profileId/:id', component: BasicInfoViewPageComponent, canActivate:[AuthGuard]},
  { path: 'editBasicInfo/:profileId/:id', component: BasicInfoEditComponent, canActivate:[AuthGuard]},
  { path: 'medicalinfo/:id', component: MedicalInfoPageComponent, canActivate:[AuthGuard]},
  { path: 'viewMedicalInfo/:profileId/:id', component: MedicalInfoViewComponent, canActivate:[AuthGuard]},
  { path: 'editMedicalInfo/:profileId/:id', component: MedicalInfoEditComponent, canActivate:[AuthGuard]},
  { path: 'medicallist/:id', component: MedicalRegisterListComponent, canActivate:[AuthGuard]},
  { path: 'medicalregister/:id', component: MedicalRegisterPageComponent, canActivate:[AuthGuard]},
  { path: 'view/:id', component: MedicalRegisterViewComponent, canActivate:[AuthGuard] },
  { path: 'update/:id', component: MedicalRegisterEditComponent, canActivate:[AuthGuard] },
  { path: 'pdf/:id', component: HtmlToPdfComponent, canActivate:[AuthGuard] },
  { path: '**', component: NotFoundPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class NameRoutingModule { }
