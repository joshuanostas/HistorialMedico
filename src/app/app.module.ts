import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { NameRoutingModule } from "./app-routing.module";
import { HttpModule } from "@angular/http";
import { Http, Headers } from "@angular/http";

//forms
import { FormsModule } from "@angular/forms";

//firebase imports
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireDatabaseModule} from "angularfire2/database";
import { AngularFireAuthModule } from "angularfire2/auth";


//components
import { AppComponent } from './app.component';
import { PrincipalIuComponent } from './components/principal-iu/principal-iu.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { BasicInfoPageComponent } from './components/crud basic-info/basic-info-page/basic-info-page.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { BasicInfoViewPageComponent } from './components/crud basic-info/basic-info-view-page/basic-info-view-page.component';
import { BasicInfoEditComponent } from './components/crud basic-info/basic-info-edit/basic-info-edit.component';
import { MedicalInfoPageComponent } from './components/crud medical-info/medical-info-page/medical-info-page.component';
import { MedicalInfoEditComponent } from './components/crud medical-info/medical-info-edit/medical-info-edit.component';
import { MedicalInfoViewComponent } from './components/crud medical-info/medical-info-view/medical-info-view.component';
import { MedicalRegisterPageComponent } from './components/crud medical-register/medical-register-page/medical-register-page.component';
import { ProfileListComponent } from './components/profile-list/profile-list.component';

//services
import { AuthService } from "./services/auth.service";
import { BasicInfoService } from "./services/basic-info.service";
import { MedicalInfoService } from "./services/medical-info.service";
import { MedicalRegisterService } from "./services/medical-register.service";

//Guards
import { AuthGuard } from "./guards/auth.guard";
import { MedicalRegisterListComponent } from './components/crud medical-register/medical-register-list/medical-register-list.component';
import { MedicalRegisterViewComponent } from './components/crud medical-register/medical-register-view/medical-register-view.component';
import { MedicalRegisterEditComponent } from './components/crud medical-register/medical-register-edit/medical-register-edit.component';
import { HtmlToPdfComponent } from './components/html-to-pdf/html-to-pdf.component';


@NgModule({
  declarations: [
    AppComponent,
    PrincipalIuComponent,
    NavbarComponent,
    RegisterPageComponent,
    LoginPageComponent,
    BasicInfoPageComponent,
    NotFoundPageComponent,
    MedicalInfoPageComponent,
    BasicInfoViewPageComponent,
    BasicInfoEditComponent,
    MedicalInfoEditComponent,
    MedicalInfoViewComponent,
    MedicalRegisterPageComponent,
    MedicalRegisterListComponent,
    MedicalRegisterViewComponent,
    MedicalRegisterEditComponent,
    ProfileListComponent,
    HtmlToPdfComponent,
  ],
  imports: [
    BrowserModule,
    NameRoutingModule,
    FormsModule,
    HttpModule,
    AngularFireModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    BasicInfoService,
    MedicalInfoService,
    MedicalRegisterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
