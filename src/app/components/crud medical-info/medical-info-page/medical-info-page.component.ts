import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { MedicalInfo } from "../../../interfaces/medical-info.interface";
import { MedicalInfoService } from "../../../services/medical-info.service";

@Component({
  selector: 'app-medical-info-page',
  templateUrl: './medical-info-page.component.html',
  styleUrls: ['./medical-info-page.component.css']
})
export class MedicalInfoPageComponent implements OnInit {

  medicalInfo:MedicalInfo = {
    profileId:  "",
    height:     "",
    weight:     "",
    blood:      "",
    allergies:  "ninguna",
    diseases:   "ninguna",
    treatments: "ninguna"
  }
  constructor(private route: ActivatedRoute,
              private medicalInfoService: MedicalInfoService) {
    this.medicalInfo.profileId = this.route.snapshot.params['id'];
   }

  ngOnInit() {
  }

  //Create medicalInfo
  save(){
    this.medicalInfoService.createMedicalInfo(this.medicalInfo, this.medicalInfo.profileId);
  }

}
