import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { MedicalInfo } from "../../../interfaces/medical-info.interface";
import { MedicalInfoService } from "../../../services/medical-info.service";

@Component({
  selector: 'app-medical-info-edit',
  templateUrl: './medical-info-edit.component.html',
  styleUrls: ['./medical-info-edit.component.css']
})
export class MedicalInfoEditComponent implements OnInit {
  medicalInfo:MedicalInfo = {
        profileId:  "",
        height:     "",
        weight:     "",
        blood:      "",
        allergies:  "ninguna",
        diseases:   "ninguna",
        treatments: "ninguna"
    }
    
  constructor(private route: ActivatedRoute,
              private medicalInfoService: MedicalInfoService) {
                this.medicalInfoService.getMedicalInfo(this.route.snapshot.params['profileId'],this.route.snapshot.params['id'])
                .subscribe(data => this.medicalInfo = data);
               }

  ngOnInit() {
  }
           
  update(){
    this.medicalInfoService.updateMedicalInfo(this.route.snapshot.params['profileId'],this.route.snapshot.params['id'],this.medicalInfo);
  }
}
