import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { MedicalInfo } from "../../../interfaces/medical-info.interface";
import { MedicalInfoService } from "../../../services/medical-info.service";

@Component({
  selector: 'app-medical-info-view',
  templateUrl: './medical-info-view.component.html',
  styleUrls: ['./medical-info-view.component.css']
})
export class MedicalInfoViewComponent implements OnInit {
  medicalInfo:MedicalInfo = {
    profileId:  "",
    height:     "",
    weight:     "",
    blood:      "",
    allergies:  "",
    diseases:   "",
    treatments: ""
}
  constructor(private route: ActivatedRoute,
              private medicalInfoService: MedicalInfoService) {
                this.medicalInfoService.getMedicalInfo(this.route.snapshot.params['profileId'],this.route.snapshot.params['id'])
                .subscribe(data => this.medicalInfo = data);
               }

  ngOnInit() {
  }

}
