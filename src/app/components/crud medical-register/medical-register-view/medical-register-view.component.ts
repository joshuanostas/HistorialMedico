import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MedicalRegister } from "../../../interfaces/medical-register.interface";
import { MedicalRegisterService } from "../../../services/medical-register.service";

@Component({
  selector: 'app-medical-register-view',
  templateUrl: './medical-register-view.component.html',
  styleUrls: ['./medical-register-view.component.css']
})
export class MedicalRegisterViewComponent implements OnInit {

  medicalRegister: MedicalRegister = {
    profileId:    "",
    title:        "",
    photo:        "",
    date:         null,
    doctorName:   "",
    hospital:     "",
    speciality:   "",
    registerType: "",
    description:  "",
    analysis:     "",
    prescription: "",
    commentary:   "",
  }

  constructor(private route: ActivatedRoute,
              private medicalRegisterService:MedicalRegisterService) { 
                this.medicalRegisterService.getMedicalRegister(this.route.snapshot.params['id'])
                .subscribe(data => this.medicalRegister = data);
              }

  ngOnInit() {
  }

}
