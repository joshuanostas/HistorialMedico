import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { MedicalRegister } from "../../../interfaces/medical-register.interface";
import { MedicalRegisterService } from "../../../services/medical-register.service";
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from 'rxjs';
import * as jsPDF from 'jspdf';
@Component({
  selector: 'app-medical-register-list',
  templateUrl: './medical-register-list.component.html',
  styleUrls: ['./medical-register-list.component.css']
})
export class MedicalRegisterListComponent implements OnInit {
  
  profileId:string;
  medicalRegisters: Observable<MedicalRegister[]>;
  filter:string = "all";
  registerArr:MedicalRegister[] = [];
  public deleteRegisterQuestion: boolean = false;
  public orderDes: boolean = false;

  constructor(private route: ActivatedRoute,
              private medicalRegisterService:MedicalRegisterService,
              public afAuth : AngularFireAuth) {
                 this.profileId = this.route.snapshot.params['id'];
              }

  ngOnInit() {
    this.afAuth.authState.subscribe(user =>{
      this.medicalRegisters = this.medicalRegisterService.getMedicalRegisters(user.uid, this.profileId);
      this.medicalRegisters.subscribe(list =>{
        for(let register of list){
            this.registerArr.push(register);
        };
      });
  });
  }             
  
  filterConsults(){
    this.filter= "Consulta"; 
  }
  filterAnalyses(){
    this.filter= "Análisis"; 
  }
  filterPrescriptions(){
    this.filter= "Prescripción"; 
  }
  filterOut(){
    this.filter= "all"; 
  }

  orderByDate(){
    if(!this.orderDes){
      this.orderDes = true;
      this.registerArr.sort(this.compareByDateDes);
    }else{
      this.orderDes = false;
      this.registerArr.sort(this.compareByDateAsc);
    }
    
  }

  compareByDateDes(a:MedicalRegister,b:MedicalRegister){
    if(a.date > b.date){
      return 1;
    }
    if(a.date < b.date){
      return -1;
    }
    return 0;
  }

  compareByDateAsc(a:MedicalRegister,b:MedicalRegister){
    if(a.date < b.date){
      return 1;
    }
    if(a.date > b.date){
      return -1;
    }
    return 0;
  }

  delete(key: string){
    this.medicalRegisterService.deleteMedicalRegister(key);
    this.registerArr = [];
  }

  showDeleteRegisterQuestion(){
    if(!this.deleteRegisterQuestion){
      this.deleteRegisterQuestion = true;
    }else{
      this.deleteRegisterQuestion = false;
    }
  }

  buscarRegistro(registro:string){
    this.registerArr = [];
    registro = registro.toLowerCase();
    this.medicalRegisters.subscribe(list =>{
        for(let register of list){
            let found =  register.title;
            found = found.toLowerCase();
            if(found.indexOf(registro) >= 0){
              this.registerArr.push(register);
            }
        };
    });
  }

}
