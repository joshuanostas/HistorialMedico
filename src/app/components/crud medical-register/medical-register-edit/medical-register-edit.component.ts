import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireStorage } from 'angularfire2/storage';
import { MedicalRegister } from "../../../interfaces/medical-register.interface";
import { MedicalRegisterService } from "../../../services/medical-register.service";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-medical-register-edit',
  templateUrl: './medical-register-edit.component.html',
  styleUrls: ['./medical-register-edit.component.css']
})
export class MedicalRegisterEditComponent implements OnInit {
  medicalRegister : MedicalRegister = {
    profileId:    "",
    title:        "",
    photo:        "",
    date:         null,
    doctorName:   "",
    hospital:     "",
    speciality:   "",
    registerType: "",
    description:  "",
    analysis:     "",
    prescription: "",
    commentary:   "",
  }

  public value:string = "";

  public analysis:string = "";
  analysisPhotoUrl: Observable<string | null>;

  public prescription:string = "";
  prescriptionPhotoUrl: Observable<string | null>;

  constructor(private medicalRegisterService:MedicalRegisterService,
              private route: ActivatedRoute,
              private storage: AngularFireStorage) {
                this.medicalRegisterService.getMedicalRegister(this.route.snapshot.params['id'])
                .subscribe(data =>{ this.medicalRegister = data;
                                    this.value = this.medicalRegister.registerType;
                                    this.analysis = this.medicalRegister.photo;
                                    this.prescription = this.medicalRegister.photo; });
               }
               
  ngOnInit() {
  }

  uploadAnalysis(event){
    const file = event.target.files[0];
    const filePath = 'registers-analysis-photos/'+ file.name;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file).then((res) => {
      this.analysisPhotoUrl = ref.getDownloadURL();
      this.analysisPhotoUrl.subscribe(aux =>{
        this.analysis=aux;
      });
    }).catch((err)=>{
      alert("algo salio mal trate recargando la pagina");
    });
  }

  uploadPrescription(event){
    const file = event.target.files[0];
    const filePath = 'registers-prescription-photos/'+ file.name;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file).then((res) => {
      this.prescriptionPhotoUrl = ref.getDownloadURL();
      this.prescriptionPhotoUrl.subscribe(aux =>{
        this.prescription=aux;
      });
    }).catch((err)=>{
      alert("algo salio mal trate recargando la pagina");
    });
  }

  paramsRequired(){
    alert("Campos obligatorios no llenados");
  }

  recuperarValue(){
    return this.value;
  }

  update(){
    if(this.value == 'Análisis'){
      this.medicalRegister.photo = this.analysis;
    }
    if(this.value == 'Prescripción'){
      this.medicalRegister.photo = this.prescription;
    }
      this.medicalRegisterService.updateMedicDocument(this.route.snapshot.params['id'],this.medicalRegister)
  }
  
}
