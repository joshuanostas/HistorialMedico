import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFireStorage } from 'angularfire2/storage';
import { MedicalRegister } from "../../../interfaces/medical-register.interface";
import { MedicalRegisterService } from "../../../services/medical-register.service";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-medical-register-page',
  templateUrl: './medical-register-page.component.html',
  styleUrls: ['./medical-register-page.component.css']
})
export class MedicalRegisterPageComponent implements OnInit {

  key:string;
  value:string;

  medicalRegister: MedicalRegister = {
    profileId:    "",
    title:        "",
    photo:        "",
    date:         null,
    doctorName:   "",
    hospital:     "",
    speciality:   "",
    registerType: "",
    description:  "",
    analysis:     "",
    prescription: "",
    commentary:   "",
  }

  public analysis:string = "";
  analysisPhotoUrl: Observable<string | null>;

  public prescription:string = "";
  prescriptionPhotoUrl: Observable<string | null>;

  constructor(private route: ActivatedRoute,
              private medicalRegisterService: MedicalRegisterService,
              private storage: AngularFireStorage) {
                this.key = this.route.snapshot.params['id'];
                this.medicalRegister.profileId = this.key;
               }

  ngOnInit() {
  }

  uploadAnalysis(event){
    const file = event.target.files[0];
    const filePath = 'registers-analysis-photos/'+ file.name;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file).then((res) => {
      this.analysisPhotoUrl = ref.getDownloadURL();
      this.analysisPhotoUrl.subscribe(aux =>{
        this.analysis=aux;
      });
    }).catch((err)=>{
      alert("algo salio mal trate recargando la pagina");
    });
  }

  uploadPrescription(event){
    const file = event.target.files[0];
    const filePath = 'registers-prescription-photos/'+ file.name;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file).then((res) => {
      this.prescriptionPhotoUrl = ref.getDownloadURL();
      this.prescriptionPhotoUrl.subscribe(aux =>{
        this.prescription=aux;
      });
    }).catch((err)=>{
      alert("algo salio mal trate recargando la pagina");
    });
  }

  recuperarValue(){
    return this.value;
  }

  paramsRequired(){
    alert("Campos obligatorios no llenados");
  }

  save(){
    if(this.value == 'Análisis'){
      this.medicalRegister.photo = this.analysis;
    }
    if(this.value == 'Prescripción'){
      this.medicalRegister.photo = this.prescription;
    }
    this.medicalRegister.profileId = this.key;
    this.medicalRegisterService.createMedicalRegister(this.medicalRegister);
  }

}
