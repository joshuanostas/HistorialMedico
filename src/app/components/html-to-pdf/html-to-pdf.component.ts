import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { BasicInfoService } from "../../services/basic-info.service";
import { BasicInfo } from "../../interfaces/basic-info.interface";
import { MedicalInfoService } from "../../services/medical-info.service";
import { MedicalInfo } from "../../interfaces/medical-info.interface";
import { MedicalRegister } from "../../interfaces/medical-register.interface";
import { MedicalRegisterService } from "../../services/medical-register.service";
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from 'rxjs';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-html-to-pdf',
  templateUrl: './html-to-pdf.component.html',
  styleUrls: ['./html-to-pdf.component.css']
})
export class HtmlToPdfComponent implements OnInit {
  public basicInfos: Observable<BasicInfo[]>;
  public basicInfosAux : Observable<BasicInfo[]>;
  public basicInfosList : BasicInfo[] = [];
  public basicInfo : BasicInfo;

  public medicalInfos: Observable<MedicalInfo[]>;  
  public medicalInfosAux : Observable<MedicalInfo[]>;
  public medicalInfosList : MedicalInfo[] = [];
  public medicalInfo: MedicalInfo;

  public medicalRegisters: Observable<MedicalRegister[]>;
  public registerArr:MedicalRegister[] = [];


  public profileId:string;

  constructor(public afAuth : AngularFireAuth,
              public basicInfoService: BasicInfoService,
              public medicalInfoService: MedicalInfoService,
              private route: ActivatedRoute,
              private medicalRegisterService:MedicalRegisterService) {
                this.profileId = this.route.snapshot.params['id'];
                //this.medicalInfo = this.medicalInfosList.find(function(a:MedicalInfo){return a.profileId === b});
              }



  ngOnInit() {
    this.afAuth.authState.subscribe(user =>{
      this.medicalRegisters = this.medicalRegisterService.getMedicalRegisters(user.uid, this.profileId);
      this.medicalRegisters.subscribe(list =>{
        for(let register of list){
            this.registerArr.push(register);
        };
      });
      this.basicInfos = this.basicInfoService.getBasicInfos(user.uid); 
      this.basicInfos.forEach(val => { 
      val.map(val =>{this.basicInfosAux = this.basicInfoService.getBasicInfos2(user.uid,val.key);
      this.basicInfosAux.subscribe(val => {this.basicInfosList.push(val[0])} ) })} );
       
      this.medicalInfos = this.medicalInfoService.getMedicalInfos(user.uid); 
      this.medicalInfos.forEach(val => { 
      val.map(val =>{this.medicalInfosAux = this.medicalInfoService.getMedicalInfos2(user.uid,val.key);
      this.medicalInfosAux.subscribe(val => {this.medicalInfosList.push(val[0])} ) })} );
  });
  }

  downloadPDF(){
    const toContent = document.querySelector('#content');
    let pdf = new jsPDF('p','pt','a4');
    pdf.setFontType('bold');
    pdf.fromHTML(toContent,15,15,{});
    pdf.save('test.pdf');
  }  

}
