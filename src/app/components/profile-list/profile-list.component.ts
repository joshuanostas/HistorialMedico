import { Component, OnInit } from '@angular/core';
import { BasicInfoService } from "../../services/basic-info.service";
import { BasicInfo } from "../../interfaces/basic-info.interface";
import { MedicalInfoService } from "../../services/medical-info.service";
import { MedicalInfo } from "../../interfaces/medical-info.interface";
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.css']
})
export class ProfileListComponent implements OnInit {
  public basicInfos: Observable<BasicInfo[]>;
  public basicInfosList : BasicInfo[] = [];
  public basicInfosAux : Observable<BasicInfo[]>;
  public medicalInfos: Observable<MedicalInfo[]>;  
  public medicalInfosList : MedicalInfo[] = [];
  public medicalInfosAux : Observable<MedicalInfo[]>;

  public changePasswordView : boolean = false;
  public newPassword : string = "";

  public deleteUserView : boolean = false;

  public deleteProfileQuestion: boolean = false;
  

  constructor(public afAuth : AngularFireAuth,
              public basicInfoService: BasicInfoService,
              public medicalInfoService: MedicalInfoService,
            ) {
   }

   delete(profileId, biKey, miKey){
    this.basicInfoService.deleteBasicInfo(profileId,biKey);
    this.medicalInfoService.deleteMedicalInfo(profileId,miKey);
    window.location.reload();
   }

   deleteUser(){
    this.afAuth.auth.currentUser.delete().then(()=>{
      window.location.reload();
      alert("Cuenta eliminada");
    }).catch((error)=>{console.log(error)});
  }

  changePassword(newPassword: string){
    console.log(newPassword);
    this.afAuth.auth.currentUser.updatePassword(newPassword).then(()=>{
      alert("Contraseña actualizada");
    }).catch((error)=>{console.log(error)});
  }

  showChangePasswordView(){
    if(!this.changePasswordView){
      this.changePasswordView = true;
    }else{
      this.changePasswordView = false;
    }
  }

  showDeleteUserView(){
    if(!this.deleteUserView){
      this.deleteUserView = true;
    }else{
      this.deleteUserView = false;
    }
  }

  showDeleteProfileQuestion(){
    if(!this.deleteProfileQuestion){
      this.deleteProfileQuestion = true;
      /*let garbage = document.querySelector("#garbage");
      garbage.classList.add("hidden");
      let question = document.querySelector("#deleteQuestion");
      question.classList.remove("hidden");*/
    }else{
      this.deleteProfileQuestion = false;
      /*let garbage = document.querySelector("#garbage");
      garbage.classList.remove("hidden");
      let question = document.querySelector("#deleteQuestion");
      question.classList.add("hidden");*/
    }
  }


  ngOnInit() {
    this.afAuth.authState.subscribe(user =>{
      this.basicInfos = this.basicInfoService.getBasicInfos(user.uid); 
      this.basicInfos.forEach(val => { 
      val.map(val =>{this.basicInfosAux = this.basicInfoService.getBasicInfos2(user.uid,val.key);
       this.basicInfosAux.subscribe(val => {this.basicInfosList.push(val[0])} ) })} );
       
       this.medicalInfos = this.medicalInfoService.getMedicalInfos(user.uid); 
       this.medicalInfos.forEach(val => { 
       val.map(val =>{this.medicalInfosAux = this.medicalInfoService.getMedicalInfos2(user.uid,val.key);
        this.medicalInfosAux.subscribe(val => {this.medicalInfosList.push(val[0])} ) })} );
   });
  }
}
