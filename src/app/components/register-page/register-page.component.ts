import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  public email: string;
  public password: string;
  constructor(
    public AuthService: AuthService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  paramsRequired(){
    alert("Llene el campo de Email y/o Contraseña");
  }
  emailInvalid(){
    alert("Formato de Email invalido");
  }
  passwordInvalid(){
    alert("Contraseña invalida")
  }

  onSubmitAddUser(){
    this.AuthService.registerUser(this.email,this.password)
    .then((res) =>{
      this.router.navigate(["/basicinfo"]);
      //window.location.reload();
    }).catch((err)=>{
      alert("Email ya existente");
      console.log(err);
    });

  }

}
