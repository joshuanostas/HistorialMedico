import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BasicInfo } from "../../../interfaces/basic-info.interface";
import { BasicInfoService } from "../../../services/basic-info.service";

@Component({
  selector: 'app-basic-info-view-page',
  templateUrl: './basic-info-view-page.component.html',
  styleUrls: ['./basic-info-view-page.component.css']
})
export class BasicInfoViewPageComponent implements OnInit {

  basicInfo : BasicInfo = {
    photo :    "",
    profileId: "",
    firstName: "",
    lastName:  "",
    date:      null,
    gender:    "",
    direction: "",
    phone:     "",
    email:     "",
  }

  constructor(private route: ActivatedRoute,
              private basicInfoService: BasicInfoService) {
                this.basicInfoService.getBasicInfo(this.route.snapshot.params['id'],this.route.snapshot.params['profileId'])
                .subscribe(data => this.basicInfo = data);
               }
               
  ngOnInit() {
  }

}
