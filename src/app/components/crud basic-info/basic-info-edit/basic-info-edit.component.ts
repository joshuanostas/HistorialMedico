import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router"
import { AngularFireStorage } from 'angularfire2/storage';
import * as firebase from 'firebase';
import { BasicInfo } from "../../../interfaces/basic-info.interface";
import { BasicInfoService } from "../../../services/basic-info.service";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-basic-info-edit',
  templateUrl: './basic-info-edit.component.html',
  styleUrls: ['./basic-info-edit.component.css']
})
export class BasicInfoEditComponent implements OnInit {

  public basicInfo:BasicInfo = {
    photo:     "",
    profileId :"",
    firstName: "",
    lastName:  "",
    date:      null,
    gender:    "",
    direction: "",
    phone:     "",
    email:     ""
  }

  profileUrl: Observable<string | null>;

  constructor(private route : ActivatedRoute,
              private basicInfoService:BasicInfoService,
              private storage: AngularFireStorage) {
                this.basicInfoService.getBasicInfo(this.route.snapshot.params['id'],this.route.snapshot.params['profileId'])
                .subscribe(data => this.basicInfo = data);
               }

  ngOnInit() {
  }

  // Update BasicInfo
  update(){
    this.basicInfoService.updateBasicInfo(this.route.snapshot.params['profileId'],this.route.snapshot.params['id'],this.basicInfo);
  }

  // Upload photo
  uploadFile(event){
    const file = event.target.files[0];
    const filePath = 'basic-infos-photos/'+ file.name;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file).then((res) => {
      this.profileUrl = ref.getDownloadURL();
      this.profileUrl.subscribe(aux =>{
        this.basicInfo.photo=aux;
      });
    }).catch((err)=>{
      alert("algo salio mal trate recargando la pagina");
    });
  }

  // Error message 
  paramsRequired(){
    alert("Campos obligatorios no llenados");
  }

}
