import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router"
import { AngularFireStorage } from 'angularfire2/storage';
import * as firebase from 'firebase';
import { BasicInfo } from "../../../interfaces/basic-info.interface";
import { Profile } from "../../../interfaces/profile.interface";
import { BasicInfoService } from "../../../services/basic-info.service";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-basic-info-page',
  templateUrl: './basic-info-page.component.html',
  styleUrls: ['./basic-info-page.component.css']
})
export class BasicInfoPageComponent implements OnInit {

  basicInfo:BasicInfo = {
    profileId: "",
    photo:     "",
    firstName: "",
    lastName:  "",
    date:      null,
    gender:    "",
    direction: "",
    phone:     "",
    email:     ""
  }

  public newPhoto:string = "http://planetalibro.net/ebooks/images/authors/anonimo.jpg";
  profileUrl: Observable<string | null>;
  
  constructor(private router : Router,
              private basicInfoService:BasicInfoService,
              private storage: AngularFireStorage){
  }
  
  ngOnInit(){
  }
  
  // Upload photo
  uploadFile(event){
    const file = event.target.files[0];
    const filePath = 'basic-infos-photos/'+ file.name;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file).then((res) => {
      this.profileUrl = ref.getDownloadURL();
      this.profileUrl.subscribe(aux =>{
        this.newPhoto=aux;
      });
    }).catch((err)=>{
      alert("algo salio mal trate recargando la pagina");
    });
  }

  // Generate profileID
  makeId() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  // Create BasicInfo
  save(){
    this.basicInfo.profileId = this.makeId();
    this.basicInfo.photo = this.newPhoto;
    this.basicInfoService.createBasicInfo(this.basicInfo, this.basicInfo.profileId);
  }

  // Error message 
  paramsRequired(){
    alert("Campos obligatorios no llenados");
  }
}
