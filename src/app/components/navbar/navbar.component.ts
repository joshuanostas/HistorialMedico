import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public isLogin: Boolean;
  public userName: string;
  public userEmail: string;

  constructor(
    public authService: AuthService,
    public afAuth : AngularFireAuth
  ) { }

  //Primer metodo que se utiliza
  ngOnInit() {
    this.authService.getAuth().subscribe(auth =>{
      if(auth){
        this.isLogin = true;
        this.userName = auth.displayName;
        this.userEmail = auth.email;
      }else{
        this.isLogin = false;
      }
    });
  }

  onClickLogout(){
    this.authService.logout();
  }
  
}
