// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBaa6etqOsHckLx1EN5_RLU2noASGtOSvs",
    authDomain: "historialmedico-74f5f.firebaseapp.com",
    databaseURL: "https://historialmedico-74f5f.firebaseio.com",
    projectId: "historialmedico-74f5f",
    storageBucket: "historialmedico-74f5f.appspot.com",
    messagingSenderId: "708967707848"
  }
};
